import os, sys

# from distutils.core import setup, Extension
# from distutils import sysconfig

from setuptools import setup, Extension, Command, find_packages
# from disutils import sysconfig


cpp_args = ['-std=c++14']
if sys.platform == 'darwin':
    cpp_args += ['-stdlib=libc++', '-mmacosx-version-min=10.7']
else:
    pass
    #cpp_args += ['-fopenmp']

ext_modules = []
ext_modules +=  [ Extension(
        'jkimg',
        [ 'python/jkimg/jkimg.cpp'],
        include_dirs=['include'],
        language='c++',
        extra_compile_args = cpp_args ) ]

ext_modules += [ Extension(
	'jkflow',
	[ 'python/jkflow/jkflow.cpp'],
	include_dirs=['include'],
	language='c++',
	extra_compile_args = cpp_args ) ]

ext_modules += [ Extension(
	'jkgpca',
	[ 'python/jkgpca/jkgpca.cpp'],
	include_dirs=['include'],
	language='c++',
	extra_compile_args = cpp_args ) ]
	

# class BuildExt(build_ext):
#     """A custom build extension for adding compiler-specific options."""
#     c_opts = {
#         'msvc': ['/EHsc'],
#         'unix': [],
#     }

#     if sys.platform == 'darwin':
#         c_opts['unix'] += ['-stdlib=libc++', '-mmacosx-version-min=10.7']

#     def build_extensions(self):
#         ct = self.compiler.compiler_type
#         opts = self.c_opts.get(ct, [])
#         if ct == 'unix':
#             opts.append('-DVERSION_INFO="%s"' % self.distribution.get_version())
#             opts.append(cpp_flag(self.compiler))
#             if has_flag(self.compiler, '-fvisibility=hidden'):
#                 opts.append('-fvisibility=hidden')
#         elif ct == 'msvc':
#             opts.append('/DVERSION_INFO=\\"%s\\"' % self.distribution.get_version())
#         for ext in self.extensions:
#             ext.extra_compile_args = opts
#         build_ext.build_extensions(self)

setup(
    name='jkai',
    version='0.0.3',
    author='Ja-Keoung Koo',
    description='Example',

    packages=find_packages(exclude=('test')),
    ext_modules=ext_modules,
)
