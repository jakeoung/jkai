/*
JKImage library for communicating with python

Rules

- The image usually follows the order of [Channel x nx x ny]
- The output argument should be positioned first.
*/

/*
JKImage library for communicating with python
Rules
- The input image usually follows the order of [Channel x ny x nx]
- 
*/

#ifndef __JKMAT_H__
#define __JKMAT_H__

// #include "jkimg.h"
#include <cmath>

#define MAX(a,b) (((a) >= (b)) ? (a) : (b))
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

// helper defininiton of indexing for numpy-given images of shape [C x H x W] column-wise
#define IDX( arr, x, y, c, ny, nx ) arr[ x + y*nx + c*ny*nx ]

// helper defininiton of indexing for Matlab-given images of shape [C x W x H] row-wise
// #define TMATLAB( I, x, y, c, ny, nx ) I[ y + x*ny + c*ny*nx ]

namespace jkmat {

template <typename Real>
void dot(Real *out, Real *A, int Arow, int Acol,
      Real *B, int Bcol)
{
    for (int i=0; i < Arow; i++) {
        for (int k=0; k < Bcol; k++) {
            Real sum = 0.;
            for (int j=0; j < Acol; j++) {
                sum += A[i*Acol + j] * B[j*Bcol + k];
            }
            out[i*Bcol + k] = sum;
        }
    }
}

template <typename Real>
Real normF(Real *A, int len)
{
    /* Frobenius norm */

    Real F=0.;
    for (int i=0; i < len; i++) {
        F += A[i]*A[i];
    }
    return sqrt(F);
}

template <typename Real>
//Function to compute the inverse of a matrix
//through Gaussian elimination
int inverse(
  Real *A,   //input matrix
  Real *A_1, //output matrix
  int N        //matrix dimension
) 
{
  Real *PASO=new Real[2*N*N];

  Real max,paso,mul;
  int i,j,i_max,k;

  for(i=0;i<N;i++){
    for(j=0;j<N;j++){
      PASO[i*2*N+j]=A[i*N+j];
      PASO[i*2*N+j+N]=0.;
    }
  }    
  for(i=0;i<N;i++)
      PASO[i*2*N+i+N]=1.;      
      
  for(i=0;i<N;i++){
    max=fabs(PASO[i*2*N+i]);
    i_max=i;
    for(j=i;j<N;j++){
       if(fabs(PASO[j*2*N+i])>max){
         i_max=j; max=fabs(PASO[j*2*N+i]);
       } 
    }

    if(max<10e-30){ 
      delete []PASO;
      return -1;
    }
    if(i_max>i){
      for(k=0;k<2*N;k++){
        paso=PASO[i*2*N+k];
        PASO[i*2*N+k]=PASO[i_max*2*N+k];
        PASO[i_max*2*N+k]=paso;
      }
    } 

    for(j=i+1;j<N;j++){
      mul=-PASO[j*2*N+i]/PASO[i*2*N+i];
      for(k=i;k<2*N;k++) PASO[j*2*N+k]+=mul*PASO[i*2*N+k];                
    }
  }
  
  if(fabs(PASO[(N-1)*2*N+N-1])<10e-30){ 
      delete []PASO;
      return -1;
  }
      
  for(i=N-1;i>0;i--){
    for(j=i-1;j>=0;j--){
      mul=-PASO[j*2*N+i]/PASO[i*2*N+i];
      for(k=i;k<2*N;k++) PASO[j*2*N+k]+=mul*PASO[i*2*N+k];     
    }
  }  
  for(i=0;i<N;i++)
    for(j=N;j<2*N;j++)
      PASO[i*2*N+j]/=PASO[i*2*N+i];  
    
  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      A_1[i*N+j]=PASO[i*2*N+j+N];

  delete []PASO;
  
  return 0;   
}

}

#endif