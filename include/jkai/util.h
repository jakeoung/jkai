#ifndef __COMMON_H__
#define __COMMON_H__

////////////////////////////////////
#define __PYDEBUG__

#ifdef __PYDEBUG__
#include <pybind11/pybind11.h>
#endif

extern void debug(std::string x)
{
#ifdef __PYDEBUG__
    pybind11::print(x);
#endif
}
/////////////////////////////////////

#endif