#ifndef __TILT_HPP__
#define __TILT_HPP_


#include <iostream>
#include <cmath>

#include <Eigen/Dense>
#include <Eigen/SVD>
#include "jkai/jkimg.h"
// #include "tilt.h"

#ifndef Dtype
#define Dtype float
#endif

#define __PYDEBUG__

#ifdef __PYDEBUG__
    #include <pybind11/pybind11.h>
    namespace py = pybind11;
    #define DEBUG py::print
#else
    #define DEBUG //
#endif

/*
bbox (Dtype[4]) : x, y, w (width) , h (height)
*/
using namespace Eigen;
typedef Eigen::Map<Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> MapMatrix;

template <typename Real>
void warp_perspective(Real* out, Real* img, Real* P, int nx, int ny,
    int nx_warp, int ny_warp, int use_inverse=1)
{
    Real P_copy [9];
    Eigen::Map<Eigen::Matrix<Real, 3, 3, Eigen::RowMajor>> P_eig(P);
    Eigen::Map<Eigen::Matrix<Real, 3, 3, Eigen::RowMajor>> P_(P_copy);
    // if (use_inverse) {
    Matrix<Real, 3, 3> I;
    I << 1, 0, 0, 0, 1, 0, 0, 0, 1;
    P_ = P_eig.colPivHouseholderQr().solve(I);

    // }

    Real M11 = P_(0,0); Real M12 = P_(0,1); Real M13 = P_(0,2);
    Real M21 = P_(1,0); Real M22 = P_(1,1); Real M23 = P_(1,2);
    Real M31 = P_(2,0); Real M32 = P_(2,1); Real M33 = P_(2,2);

    bool denom_zero= false;
    for (int y=0; y < ny_warp; y++) {
        Real fy = (Real)y;
        for (int x=0; x < nx_warp; x++) {
            Real fx = (Real)x;
            Real denom = ((M31 * fx) + (M32 * fy) + M33);
            if (fabs(denom) > 1e-8) {
                denom = 1.f / denom;
            } else {
                denom = 0.f;
                denom_zero = true;
            }
            Real xx = (Real)((M11 * fx) + (M12 * fy) + M13) * denom;
            Real yy = (Real)((M21 * fx) + (M22 * fy) + M23) * denom;

            // interpolation
            Real val = jkimg::interp2_bicubic_at(img, xx, yy, nx, ny, (Real)0.);
            out[y*nx_warp + x] = val;
        }
    }
    if (denom_zero)
        DEBUG("denom is zero");
}


void perspective_transform(Dtype* out, Dtype* P_, Dtype* pts, int n)
    /* 
    Args
        - P_ (Dtype*, [3 x 3]) : matrix
        - pts (Dtype*, [n x 2]) : points consisting of (x,y) coordinates
        - n (int) : number of points
    Output
        - out (Dtype*) : 
    */
{
    Map<Matrix<Dtype, 3, 3, RowMajor>> P(P_);
    VectorXf coord(3);

    coord[2] = 1;
    for (int i=0; i < n; i++) {
        coord[0] = pts[2*i];
        coord[1] = pts[2*i+1];

        auto coord_ = P * coord;
        
        out[2*i] = coord_[0];
        out[2*i+1] = coord_[1];
    }
}

void compute_perspective_transform(Dtype* out, Dtype* pts1, Dtype* pts2)
{
    MatrixXf A(8, 8);
    Eigen::VectorXf b(8);

    for (int i=0; i < 4; i++) {
        A(i, 0) = A(i+4, 3) = pts1[2*i];
        A(i, 1) = A(i+4, 4) = pts1[2*i+1];
        A(i, 2) = A(i+4, 5) = 1;
        A(i, 3) = A(i, 4) = A(i, 5)=
        A(i+4, 0) = A(i+4, 1) = A(i+4, 2) = 0;
        A(i, 6) = -pts1[2*i] * pts2[2*i];
        A(i, 7) = -pts1[2*i+1] * pts2[2*i];
        A(i+4, 6) = -pts1[2*i] * pts2[2*i+1];
        A(i+4, 7) = -pts1[2*i+1] * pts2[2*i+1];

        b[i] = pts2[2*i];
        b[i+4] = pts2[2*i+1];
    }

    // DEBUG(pts1[0], pts1[1]);
    Eigen::VectorXf x = A.colPivHouseholderQr().solve(b);
    // c_ = c.data();
    for (int i=0; i < 8; i++) {
        out[i] = x[i];
        // DEBUG(out[i], x[i]);
    }
    out[8] = 1.;
}

void transform_bbox(Dtype* out, Dtype* img, Dtype* T, int* bbox, int nx, int ny)
{
    /*
    Args
        - img (Dtype* [ny*nx]) : image
        - T (Dtype* [3x3]) : transformation matrix
        - bbox (int* [4]) : patch (x, y, width, height) where (x, y)
                            are the top left coordinate
        - nx, ny (int) : image size

    Output
        - out (Dtype) : transformed patch of shape (width, height)
    */
    
    Dtype w = (Dtype)bbox[2];
    Dtype h = (Dtype)bbox[3];

    Dtype pts [8];
    Dtype pts_tr[8];
    Dtype lux = (Dtype)bbox[0];
    Dtype luy = (Dtype)bbox[1];

    pts[0] = lux;       pts[1] = luy;
    pts[2] = lux + w;   pts[3] = luy;
    pts[4] = lux;       pts[5] = luy + h;
    pts[6] = lux + w;   pts[7] = luy + h;

    /** step1 : center the data **/
    Dtype cx = lux + (w/2.);
    Dtype cy = luy + (h/2.);

    for (int i=0; i < 4; i++) {
        pts_tr[i*2] = pts[i*2] - cx;
        pts_tr[i*2+1] = pts[i*2+1] - cy;
    }

    /** step2 : transform pts **/
    perspective_transform(pts_tr, T, pts_tr, 4);

    /** step3 : make transformed points to the image of shape w, h **/
    for (int i=0; i < 4; i++) {
        pts_tr[i*2] += w/2;
        pts_tr[i*2+1] += h/2;
    }

    /** step4 : compute perspective transformation matrix **/
    auto P = std::make_unique<Dtype[]>(9);
    compute_perspective_transform(P.get(), pts, pts_tr);

    // /** step5 : warping **/
    warp_perspective(out, img, P.get(), nx, ny, bbox[2], bbox[3], 1);
}

Dtype get_frob_norm(Dtype* inp, int nx, int ny)
    /* Brief description.
    Args
        - inp (Dtype*) : matrix
    Output
        - frob (Dtype) : Frobenius norm
    */
{
    Map<Matrix<Dtype, Dynamic, Dynamic, RowMajor>> T(inp, ny, nx);
    JacobiSVD<Matrix<Dtype, Dynamic, Dynamic, RowMajor>> svd( T, Eigen::ComputeThinU | Eigen::ComputeThinV );

    auto sigma = svd.singularValues();
    return (Dtype)sigma.sum();
}

void compute_jacobian(Dtype* Jinv, Dtype* J, Dtype* Ix, Dtype* Iy, Dtype* Tau, int nx, int ny)
    /* Jacobian of image w.r.t. homography matrix
    J = \nabla I (dx)/(d\tau)

    Args:
        - bbox (flot array of 4 length) : x-y coordinate, w(width), h(height)
        - Ix : image derivative w.r.t. x
        - Iy : image derivative w.r.t. y
        - Tau [3 x 3] : homography matrix

    Output:
        - J (tensor of shape [w x h x 8])
        - Jinv : Pseudo-inverse of J
    */
{
    for (int y=0; y < ny; y++) {
        for (int x=0; x < nx; x++) {
            Dtype jacob;
            Dtype N1 = Tau[0]*x + Tau[1]*y + Tau[2]; // homography proection
            Dtype N2 = Tau[3]*x + Tau[4]*y + Tau[5];
            Dtype N = Tau[6]*x + Tau[7]*y + 1;

            int idx = y*nx + x;
            int idxj = 8*idx;

            J[idxj] = Ix[idx] * (Dtype)x / N;
            J[idxj+1] = Ix[idx] * (Dtype)y / N;
            J[idxj+2] = Ix[idx] / N;
            J[idxj+3] = Iy[idx] * (Dtype)x / N;
            J[idxj+4] = Iy[idx] * (Dtype)y / N;
            J[idxj+5] = Iy[idx] / N;
            J[idxj+6] = Ix[idx] * (-N1 / (N*N) * (Dtype)x)
                        + Iy[idx] * (-N2 / (N*N) * (Dtype)x);
            J[idxj+7] = Ix[idx] * (-N1 / (N*N) * (Dtype)y)
                        + Iy[idx] * (-N2 / (N*N) * (Dtype)y);
        }
    }

    MapMatrix J_(J, nx*ny, 8);
    MapMatrix Jinv_(Jinv, 8, nx*ny);

    // auto Jt = J_.transpose();
    // auto JtJ = Jt * J_;
    // auto I = Eigen::Matrix<Dtype, 8, 8>::Identity();
    // auto JtJinv = JtJ.colPivHouseholderQr().solve(I);
    // Jinv_ = JtJinv * Jt;

    Jinv_ = (J_.transpose() * J_).colPivHouseholderQr().solve(
                Eigen::Matrix<Dtype, 8, 8>::Identity()
            ) * J_.transpose();
}

Dtype soft_shrinkage(Dtype x, Dtype mu)
{
    if (x > mu) return x - mu;
    else if (x < -mu) return x + mu;
    else return 0;
}

void prox_nuclear(Dtype* out, Dtype* A, Dtype eps, int nx, int ny)
{
    MapMatrix out_(out, ny, nx);
    MapMatrix mat(A, ny, nx);

    Eigen::JacobiSVD<Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>
        svd(mat, Eigen::ComputeThinU | Eigen::ComputeThinV );
    
    auto singular = svd.singularValues();

    Matrix<Dtype, Dynamic, Dynamic, RowMajor> Sigma(nx, nx);
    
    for (int y=0; y < nx; y++) {
        for (int x=0; x < nx; x++) 
            Sigma(x,y) = 0;
        
        Sigma(y,y) = soft_shrinkage(singular[y], eps);
    }

    out_ = svd.matrixU() * Sigma * svd.matrixV();
}

#endif