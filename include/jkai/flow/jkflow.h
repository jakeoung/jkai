#include <iostream>
#include <memory>

#ifndef __JKFLOW_H__
#define __JKFLOW_H__

#ifndef Dtype
#define Dtype float
#endif

class JKFlow {
protected:
    Dtype *I1, *I2;
    std::unique_ptr<Dtype[]> m_u, m_v;
    // Dtype *u, *v;
    int m_nx, m_ny, m_nc;
    int m_niters;

public:
    JKFlow() {}

    JKFlow(Dtype* I1, Dtype* I2, int nx, int ny, int nc,
            int niters)
        : I1(I1), I2(I2), m_nx(nx), m_ny(ny), m_nc(nc),
            m_niters(niters)
        {
            // m_u = std::unique_ptr<Dtype>( new Dtype[ nx*ny*nc ] );
            m_u = std::make_unique<Dtype[]> (nx*ny*nc);
            m_v = std::make_unique<Dtype[]> (nx*ny*nc);
        }
    
    // virtual void compute_flow() {}
    inline Dtype* get_u_ptr() {return m_u.get();}
    inline Dtype* get_v_ptr() {return m_v.get();}
    
    virtual void compute_flow() {}
};


class LKInverse : public JKFlow {
    int nparams;
    std::unique_ptr<Dtype[]> m_p;

public:
    LKInverse(Dtype* I1, Dtype* I2, int nx, int ny, int nc,
        int niters, int nscales, int nparams)
        : JKFlow {I1, I2, nx, ny, nc, niters}
        , nparams(nparams)
        {
            m_p = std::make_unique<Dtype[]>(nparams);
        }

    void compute_flow_scale(int nx, int ny, int nc);
    virtual void compute_flow() override;
};

class HS : public JKFlow {
    int m_nscales;
    
public:
    HS(Dtype* I1, Dtype* I2, int nx, int ny, int nc,
        int niters, int nscales)
        : JKFlow {I1, I2, nx, ny, nc, niters}
        {
            m_nscales = nscales;
        }
    
    // virtual void compute_flow() override;
    void compute_flow() override;
};


class EpicFlow : public JKFlow {
    Dtype *edges;
    Dtype *matches;

public:
    EpicFlow(Dtype* I1, Dtype* I2, int nx, int ny, int nc,
            int niters, Dtype* edges, Dtype* matches)
        : JKFlow {I1, I2, nx, ny, nc, niters}, edges{edges}, matches{matches}
        {

        }
        
    virtual void compute_flow() override;
};

#endif