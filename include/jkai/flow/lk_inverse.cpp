#include <iostream>
#include "flow/jkflow.h"
#include "jkimg.h"
#include "jkmat.h"

#include <Eigen/Dense>

#define __PYDEBUG__

#ifdef __PYDEBUG__
#include <pybind11/pybind11.h>

namespace py = pybind11;

#endif

template <int num_rows = Eigen::Dynamic, int num_cols = Eigen::Dynamic>
struct EigenTypes {
typedef Eigen::Map <
    Eigen::Matrix<Dtype, num_rows, num_cols, Eigen::RowMajor> >
MatrixRef;
};

/* See http://www.ipol.im/pub/art/2016/153/
#define TRANSLATION_TRANSFORM 2
#define EUCLIDEAN_TRANSFORM   3
#define SIMILARITY_TRANSFORM  4
#define AFFINITY_TRANSFORM    6
#define HOMOGRAPHY_TRANSFORM  8
*/
void compute_jacobian(Dtype* J, int nparams, int nx, int ny)
{
    switch (nparams) {
        default:
        case 2:
            for (int i=0; i < nx*ny; i++) {
                int c = 2*i*nparams;
                J[c] = 1.0; J[c+1] = 0.0;
                J[c+2] = 0.0; J[c+3] = 1.0;
            }
            break;
    }
}

//Multiplication of the transpose of a matrix and itself
//B should be initialized to zero outside
// b_{ij} = \sum_k a_{ik} a_{kj}
// where i:1~m, j:1~m
void AtA(Dtype *B, const Dtype *A, int n, int m)
{
  for(int i=0; i<m; i++)
    for(int j=0; j<m; j++)
      for(int k=0; k<n; k++)
        B[i*m+j]+=A[i*m+k]*A[k*m+j];
}


//Multiplication of the transpose of a matrix and a vector
//p should be initialized to zero outside

void Atb(Dtype* out, Dtype *A, Dtype *b, int n, int m)
{
  for(int i=0; i<m; i++)
    for(int j=0; j<n; j++)
      out[i]+=A[j*m+i]*b[j];
}

//Multiplication of a square matrix and a vector
void Axb(Dtype* out, Dtype *A, Dtype *b, int n)
{
  for(int i=0; i<n; i++)
  {
    Dtype sum=0.;
    for(int j=0; j<n; j++)
      sum+=A[i*n+j]*b[j];
    
    out[i]=sum;
    // py::print(sum);
  }   
}


// See Javier Sánchez Pérez
void hessian(Dtype *H, Dtype *dIJ, int nparams, int nx, int ny, int nc)
{
    // init. the hessian to zero
    for (int k=0; k < nparams*nparams; k++) {
        H[k] = 0.;
    }

    // calculate the hessian in a neighbor window
    for (int y=0; y < ny; y++) {
        for (int x=0; x < nx; x++) {
            AtA( H, &(dIJ[(y*nx + x)*nc*nparams]), nc, nparams);
        }
    }
}

void hessian_inverse(Dtype *Hinv, Dtype *H, int nparams)
{
    if(jkmat::inverse(H, Hinv, nparams) == -1) {
        //if the matrix is not invertible, set parameters to 0
        for(int i=0; i<nparams*nparams; i++)
            Hinv[i]=0;
        throw std::runtime_error("! H is not invertible");
    }
}

void LKInverse::compute_flow()
{
    LKInverse::compute_flow_scale(m_nx, m_ny, m_nc);
}

void LKInverse::compute_flow_scale(int nx, int ny, int nc)
{
    double data[2][2];
    
    Dtype TOL = 0.001;
    Dtype error = 1.0;
    auto u = m_u.get();
    auto v = m_v.get();

    auto p = m_p.get();
    
    auto Ix_ = std::make_unique<Dtype[]>(nx*ny*nc);
    auto Iy_ = std::make_unique<Dtype[]>(nx*ny*nc);
    auto Iw_ = std::make_unique<Dtype[]>(nx*ny*nc);
    auto It_ = std::make_unique<Dtype[]>(nx*ny*nc);

    auto Ix = Ix_.get();
    auto Iy = Iy_.get();
    auto Iw = Iw_.get();
    auto It = It_.get();

    auto J_ = std::unique_ptr<Dtype>(new Dtype[2*nparams*nx*ny]);
    auto H_ = std::unique_ptr<Dtype>(new Dtype[nparams*nparams]);
    // auto Hinv_ = std::unique_ptr<Dtype>(new Dtype[nparams*nparams]);
    auto dIJ_ = std::unique_ptr<Dtype>(new Dtype[nx*ny*nc*nparams]);
    auto dp_  = std::unique_ptr<Dtype>(new Dtype[nparams]);
    auto b_   = std::unique_ptr<Dtype>(new Dtype[nparams]);

    auto J = J_.get();
    auto H = H_.get();
    // auto Hinv = Hinv_.get();
    auto dIJ = dIJ_.get();
    auto dp = dp_.get();
    auto b  = b_.get();

    // compute the gradient
    jkimg::compute_gradients(Ix, Iy, I1, 0, nx, ny, nc);

    // compute the Jacorbian
    compute_jacobian(J, nparams, nx, ny);

    // compute the steepest descent images of DI' * J
    // steepest_descent_images();
    int k = 0;
    for (int y=0; y < ny; y++) {
        for (int x=0; x < nx; x++) {
            for (int c=0; c < nc; c++) {

                int idxp = x + y*nx;
                int idxc = idxp + c*nx*ny;
                for (int p=0; p < nparams; p++) {
                    dIJ[k++] = Ix[idxc] * J[2*idxp*nparams + p] +
                               Iy[idxc] * J[2*idxp*nparams + p + nparams];
                }
            }
        }
    }

    // compute the Hessian matrix
    py::print("~compute hessian matrix");
    hessian(H, dIJ, nparams, nx, ny, nc);

    py::print("~compute inverse hessian matrix");
    // hessian_inverse(Hinv, H, nparams);

    // Eigen::Map<Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> > H_eigen(H);
    auto H_inv = Eigen::Map<Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> > (H, nparams, nparams);
    int a=1;
    // EigenTypes.MatrixRef H_eigen(H, nparams, nparams);

    H_inv = H_inv.inverse();
    // auto Hinv = Hinv_.data();

    for (int iter=0; iter < m_niters; iter++) {
        // warp
        py::print("~interpolation");
        jkimg::interp2_bicubic_params(Iw, I2, p, nparams, nx, ny, nc, (Dtype)0.);

        // compute I1 - I2w
        for (int i=0; i < nx*ny*nc; i++) {
            It[i] = Iw[i] - I1[i];
        }

        py::print("~compute b");
        for (int i=0; i < nparams; i++) {
            b[i] = 0;
        }

        // compute b = sum(dIJ' * It)
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                Atb(b, &(dIJ[y*nx+x]), &(It[y*nx+x]), nc, nparams);
            }
        }

        py::print(b[0], b[1], b[2]);

        // solve equation and get increment of the motion
        py::print("~solve equation");
        error = 0.;
        
        Axb(dp, H_inv.data(), b, nparams);

        for (int i=0; i < nparams; i++) {
            error += dp[i] * dp[i];
        }
        
        // update_transform(p, dp)
        for (int i=0; i < nparams; i++)
            p[i] -= dp[i];

        py::print("@iter: ", iter, "error: ", error);

        if (error < TOL) {
            break;
        }
    }
}