/*
JKImage library for communicating with python

Rules

- The image usually follows the order of [Channel x nx x ny]
- The output argument should be positioned first.
*/

/*
JKImage library for communicating with python
Rules
- The input image usually follows the order of [Channel x ny x nx]
- 
*/
#ifndef __JKIMG_H__
#define __JKIMG_H__

#include <cmath>
#include <iostream>

#define MAX(a,b) (((a) >= (b)) ? (a) : (b))
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

// helper defininiton of indexing for numpy-given images of shape [C x H x W] column-wise
#define IDX( arr, x, y, c, ny, nx ) arr[ x + y*nx + c*ny*nx ]

// helper defininiton of indexing for Matlab-given images of shape [C x W x H] row-wise
// #define TMATLAB( I, x, y, c, ny, nx ) I[ y + x*ny + c*ny*nx ]

namespace jkimg {

int neumann_bc(int x, int nx, bool *isOut)
{
	if (x < 0) {
		x = 0;
	}
	else if (x >= nx) {
		x = nx - 1;
	}

    if (x < -2 || x >= nx+2)
        *isOut = true;

	return x;
}

//////// interpolation for optical flow
template <typename Real>
Real interp_cubic_cell(Real v[4], Real x)
{
    return v[1] + 0.5 * x * (v[2] - v[0] +
        x * (2.0 * v[0] - 5.0 * v[1] + 4.0 * v[2] - v[3] +
        x * (3.0 * (v[1] - v[2]) + v[3] - v[0])));
}

template <typename Real>
Real interp2_bicubic_at(const Real *I, Real xx, Real yy, int nx, int ny, Real bdy=(Real)0.)
{
	int sx = (xx < 0) ? -1 : 1;
	int sy = (yy < 0) ? -1 : 1;

	bool isOut[1] = { false };
	
	// neumann boundary
	int mx	= neumann_bc((int)xx - sx    , nx, isOut);
	int x   = neumann_bc((int)xx         , nx, isOut);
	int px  = neumann_bc((int)xx + sx    , nx, isOut);
	int ppx = neumann_bc((int)xx + 2 * sx, nx, isOut);

    int my  = neumann_bc((int)yy - sy    , ny, isOut);
    int y   = neumann_bc((int)yy         , ny, isOut);
    int py  = neumann_bc((int)yy + sy    , ny, isOut);
    int ppy = neumann_bc((int)yy + 2 * sy, ny, isOut);

    if (*isOut) {
        return (Real)bdy;
    }
    
    Real p11 = I[mx + my * nx];
    Real p12 = I[x + my * nx];
    Real p13 = I[px + my * nx];
    Real p14 = I[ppx + my * nx];

    Real p21 = I[mx + y * nx];
    Real p22 = I[x + y * nx];
    Real p23 = I[px + y * nx];
    Real p24 = I[ppx + y * nx];

    Real p31 = I[mx + py * nx];
    Real p32 = I[x + py * nx];
    Real p33 = I[px + py * nx];
    Real p34 = I[ppx + py * nx];

    Real p41 = I[mx + ppy * nx];
    Real p42 = I[x + ppy * nx];
    Real p43 = I[px + ppy * nx];
    Real p44 = I[ppx + ppy * nx];

    Real p[4][4] = {
        { p11, p21, p31, p41 },
        { p12, p22, p32, p42 },
        { p13, p23, p33, p43 },
        { p14, p24, p34, p44 }
    };

    Real v[4];
    v[0] = interp_cubic_cell(p[0], yy - y);
    v[1] = interp_cubic_cell(p[1], yy - y);
    v[2] = interp_cubic_cell(p[2], yy - y);
    v[3] = interp_cubic_cell(p[3], yy - y);

    return interp_cubic_cell(v, xx - x);
}

// Original author: Javier Sánchez Pérez <jsanchez@dis.ulpgc.es>
template <typename Real>
void interp2_bicubic(
			Real*	out,
	const	Real*	I,
	const	Real*	u,
	const	Real*	v,
	const	int		nx,
    const	int		ny,
    const	Real	extra
	// const	Real*	extra
)
{
    #pragma omp parallel for collapse(2)
	for (int y = 0; y < ny; y++) {
		for (int x = 0; x < nx; x++) {
			int idx = x + y * nx;
			Real xx = x + u[idx];
			Real yy = y + v[idx];

            out[idx] = interp2_bicubic_at(I, xx, yy, nx, ny);
            if (out[idx] < 0) {
                out[idx] = extra;
                // out[idx] = extra[idx];
            }
		}
	}
}

template <typename Real>
void transform_by_params(Real &xx, Real &yy, const Real *params,
     int x,  int y, const int nparams)
{
    switch (nparams) {
        default: case 2:
            xx = x + params[0];
            yy = y + params[1];
            break;
    }
}

template <typename Real>
void interp2_bicubic_params(
			Real*	out,
	const	Real*	I,
	const	Real*	params,
        	int	    nparams,
		    int		nx,
            int		ny,
            int		nc,
	        Real    extra
)
{
    #pragma omp parallel for collapse(2)
    for (int c = 0; c < nc; c++) {
        for (int y = 0; y < ny; y++) {
            for (int x = 0; x < nx; x++) {
                int idx = x + y * nx;
                int idxc = idx + nx*ny*c;

                Real xx, yy;
                transform_by_params(xx, yy, params, x, y, nparams);

                // Real xx = x + u[idx];
                // Real yy = y + v[idx];

                out[idxc] = interp2_bicubic_at(I+nx*ny*c, xx, yy, nx, ny);
                if (out[idxc] < 0) {
                    out[idxc] = extra;
                }
            }
        }
    }
}

// template<typename Real>
// void interp2_bilinear(
// 
// )

// template<typename Real>
// void compute_gradient_at(
//             Real* I_x,

// )

template<typename Real>
void compute_gradient(
            Real*  I_x,
            Real*  I_y,
	const	Real*  I,
	const	int     method,
    const   int     nx,
    const   int     ny
	)
{
	// forward difference
	if (method == 1) {
        #pragma omp parallel for collapse(2)
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                // w.r.t. x
                Real valNow, valNext;
                valNow = I[x + y * nx];
                
                if (x+1 == nx)
                    valNext = I[x+y*nx];
                else
                    valNext = I[x+1+y*nx];
                
                I_x[x+y*nx] = valNext - valNow;

                // w.r.t. y
                if (y+1 == ny)
                    valNext = I[x+y*nx];
                else
                    valNext = I[x + (y+1) * nx];

                I_y[x+y*nx] = valNext - valNow;
            }
        }
	}
	// centered difference
	else if (method == 0) {
		#pragma omp parallel for collapse(2)
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                // w.r.t. x
                Real valxp, valxm, valyp, valym;
                int xp, xm, yp, ym;

                // xp: xplus, xm: xminus
                xp = MIN(x+1, nx-1);
                xm = MAX(x-1, 0);
                yp = MIN(y+1, ny-1);
                ym = MAX(y-1, 0);

                valxp = I[xp + y * nx];
                valxm = I[xm + y * nx];
                valyp = I[x + yp * nx];
                valym = I[x + ym * nx];

                I_x[x+y*nx] = 0.5 * (valxp - valxm);
                I_y[x+y*nx] = 0.5 * (valyp - valym);
            }
        }
	}
}

template<typename Real>
void compute_gradients(
            Real*  I_x,
            Real*  I_y,
	const	Real*  I,
	const	int     method,
    const   int     nx,
    const   int     ny,
    const   int     nc
	)
{
	// forward difference
    for (int c=0; c < nc; c++) {
        int offset = nx*ny*c;
        compute_gradient(I_x+offset, I_y+offset, I, method, nx, ny);
    }
}

// compute divergence with backward difference
template <typename Real>
void compute_divergence(
    	    Real*  out,
    const   Real*  I1,  
    const   Real*  I2,
    const   int     nx,
    const   int     ny
	)
{
	// backward difference
	#pragma omp parallel for collapse(2)
    for (int y=0; y < ny; y++) {
        for (int x=0; x < nx; x++) {
            Real val1, val1xm, val2, val2ym, dx, dy;
            int xm, ym, idx;

            idx = x + y * nx;

            // xm: xminus
            xm = MAX(x-1, 0);
            ym = MAX(y-1, 0);

            val1 = I1[x+y*nx];
            val2 = I2[x+y*nx];

            val1xm = I1[xm+y*nx];
            val2ym = I2[x+ym*nx];

            if (x == 0) {
                val1xm = 0;
            } else if (x+1 == nx) {
                val1 = 0;
            }

            if (y == 0) {
                val2ym = 0;
            } else if (y+1 == ny) {
                val2 = 0;
            }

            dx = val1 - val1xm;
            dy = val2 - val2ym;

            out[x+y*nx] = dx + dy;
        }
    }
}

// solve: A x = b by Gauss-Seidel: A = (I - pLHS * Laplacian), b = pRHS
template <typename Real>
void compute_gauss_seidel(
            Real*  pData,
    const   Real*  pLHS,
    const   Real*  pRHS,
    const   int     nIterGS,
    const   int     nx,
    const   int     ny)
{
    Real  hh = 0.25;
    
    for(int i = 0; i < nIterGS; i++)
    {
        #pragma omp parallel for collapse(2)
        for(int y = 0; y < ny; y++)
        {
            for(int x = 0; x < nx; x++)
            {
                Real dX2, dA, dB, dAxp, dAxm, dAyp, dAym, dXxp, dXxm, dXyp, dXym;
                int xm, xp, ym, yp, idx;
                
                idx = x + y * nx;
                
                dA  = pLHS[idx];
                dB  = pRHS[idx];

                // getNeighbour4
                xm = MAX(x-1, 0);
                xp = MIN(x+1, nx-1);
                
                ym = MAX(y-1, 0);
                yp = MIN(y+1, ny-1);

                dAxp = pLHS[xp + y * nx];
                dAxm = pLHS[xm + y * nx];
                dAyp = pLHS[x + yp * nx];
                dAym = pLHS[x + ym * nx];
                
                dXxp = pData[xp + y * nx];
                dXxm = pData[xm + y * nx];
                dXyp = pData[x + yp * nx];
                dXym = pData[x + ym * nx];

                dX2 = hh * dB + (dAxp * dXxp + dAxm * dXxm + dAyp * dXyp + dAym * dXym);
                dX2 = dX2 / (hh + 4.0 * dA);
                
                pData[idx] = dX2;
            }
        }
    }
}

template <typename Real>
Real maxFromArray(Real* arr, int size)
{
    Real dMax = arr[0];
    for (int i=1; i < size; i++) {
        if (arr[i] > dMax) {
            dMax = arr[i];
        }
    }
    return dMax;
}

template <typename Real>
Real minFromArray(Real* arr, int size)
{
    Real dMin = arr[0];
    for (int i=1; i < size; i++) {
        if (arr[i] < dMin) {
            dMin = arr[i];
        }
    }
    return dMin;
}

int neumann_bc_x(int c, int r, int ny, int nx, bool *isOut, const int* R)
{
    if (c < 0 ) {
        c = 0;
        *isOut = true;
    }
    else if (c > 0 && r > 0 && r < ny && R[ c-1 + r*nx] == 0 ) {
        //c = 0;
        *isOut = true;
    }
    else if (c >= nx) {
        c = nx - 1;
        *isOut = true;
    }
    else if (c + 1 < nx && r > 0 && r < ny && R[ c+1 + r*nx] == 0) {
        //c = c;
        *isOut = true;
    }
    return c;
}

// assume that c is within region
int neumann_bc_y(int c, int r, int ny, int nx, bool *isOut, const int* R)
{
    if (r < 0) {
        r = 0;
        *isOut = true;
    }
    else if (r > 0 && R[ c + (r-1)*nx] == 0) {
        //r = 0;
        *isOut = true;
    }
    else if (r >= ny) {
        r = ny - 1;
        *isOut = true;
    }
    else if (r+1 < ny &&  R[ c + (r+1)*nx] == 0) {
        //c = c;
        *isOut = true;
    }
    return r;
}

// return gaussian kernel
template <typename Real>
auto gaussian_kernel(Real* kernel, int n, Real sigma)
{
    // e^( -(x^2 + y^2) / (2sigma^2) )
    Real sum=0., val;
    for (int y=0; y < n; y++) {
        for (int x=0; x < n; x++) {
            int xx = x - n/2;
            int yy = y - n/2;
            val = exp( -(xx*xx + yy*yy) / (2.0*sigma*sigma) );
            sum += val;
            kernel[y*n + x] = val;
        }
    }

    // normalization
    for (int i=0; i < n*n; i++)
        kernel[i] /= sum;
}

/*
2D convolution of an image [nx x ny] with a kernel of size [n x n]
The output image will be the same as the original image.

CURRENTLY, nc=1 IS ONLY SUPPORTED.
Args:
    kernel (Real*) : [K x K x nc] kernel
    K (int) : kernel size for the kernel of KxK
    padding : 0 if zero padding, 1 if reflection
*/
template <typename Real>
void corr2d(
          Real *out,
    const Real *inp, const int nx, const int ny, const int nc,
    const Real *kernel, const int nKx, const int nKy,
    const int padding)
{
    // assert(nc == 1);
    // HH = (H + 2 * padding - K) / stride + 1
    int K = nKx*nKy;
    int nx_out = nx;
    int ny_out = ny;

    int Kx_pad = (int)(nKx / 2);
    int Ky_pad = (int)(nKy / 2);
    int nx_pad = nx + 2 * Kx_pad;
    int ny_pad = ny + 2 * Ky_pad;
    auto inp_pad = std::make_unique<Real[]>(nc * nx_pad * ny_pad * sizeof(Real));

    /*** padding ***/
    #pragma omp parallel for collapse(3)
    for (int c=0; c < nc; c++) {
        for (int y=0; y < ny_pad; y++) {
            for (int x=0; x < nx_pad; x++) {
                int idx = c*nx_pad*ny_pad + y*nx_pad + x;

                int x_new;
                int y_new;

                x_new = x-Kx_pad;
                y_new = y-Ky_pad;

                if (padding == 1) {
                    // reflection boundary
                    // check y_new = -1, -2, ...
                    // check y_new = ny, ny+1, ny+2, ...
                    if (x_new < 0)
                        x_new = -x_new - 1;
                    else if (x_new >= nx)
                        x_new = -(x_new - nx) + nx-1;
                    
                    if (y_new < 0)
                        y_new = -y_new - 1;
                    else if (y_new >= ny)
                        y_new = -(y_new - ny) + ny-1;

                    inp_pad[idx] = inp[c*nx*ny + y_new*nx + x_new ];
                
                } else if (padding == 0) {
                    // zero padding
                    if (x_new < 0 || x_new >= nx || y_new < 0 || y_new >= ny) {
                        inp_pad[idx] = 0;
                    } else {
                        inp_pad[idx] = inp[c*nx*ny + y_new*nx + x_new ];
                    }
                }
                
            }
        }
    }

    /*** im2col ***/
    auto cols = std::make_unique<Real[]>(nc*nx*ny*K*K*sizeof(Real));

    #pragma omp parallel for collapse(5)
    for (int c=0; c < nc; c++) {
        for (int yy=0; yy < ny_out; yy++) {
            for (int xx=0; xx < nx_out; xx++) {
                for (int ky=0; ky < nKy; ky++) {
                    for (int kx=0; kx < nKx; kx++) {
                        int y = c*nKx*nKy + ky * nKx + kx;
                        int x = yy * nx_out + xx;
                        cols[y*(ny_out*nx_out) + x] = inp_pad[c*nx_pad*ny_pad + (yy+ky)*nx_pad + (xx+kx) ];
                    }
                }
            }
        }
    }

    /*** matrix multiplication of kernel * cols ***/
    // kernel shape : [c x K]
    // cols   shape : [K x nx_out*ny_out]
    
    #pragma omp parallel for collapse(2)
    for (int c=0; c < nc; c++) {
        for (int k=0; k < nx_out*ny_out; k++) {
            Real sum = 0.;
            for (int j=0; j < K; j++) {
                sum += kernel[j] * cols[j*nx_out*ny_out + k];
            }
        
            out[k] = sum;
        }
    }

}


template<typename Real>
void detect_edge(
            Real*  edge,
    const	Real*  I,
	const	int    method,
    const   int    nx,
    const   int    ny,
    const   Real   threshold,
    const   Real   sigma=0.5
	)
{
	// sobel operator
    // See http://www.ipol.im/pub/art/2015/35/
    if (method == 1) {
        Real fy[9] = {-1,-2,-1, 0,0,0,  1,2,1};
        Real fx[9] = {-1,0,1,  -2,0,2, -1,0,1};
        // normalization
        for (int i=0; i < 9; i++) {
            fx[i] /= 8.0;
            fy[i] /= 8.0;
        }
        
        // convolution
        auto I_x = std::make_unique<Real[]>(nx*ny);
        auto I_y = std::make_unique<Real[]>(nx*ny);

        // jkimg::corr2d<Real>(I_x, I, nx, ny, 1, fx, 3, 3, 1);
        // jkimg::corr2d<Real>(I_y, I, nx, ny, 1, fx, 3, 3, 1);
        
        corr2d<Real>(I_x.get(), I, nx, ny, 1, fx, 3, 3, 1);
        corr2d<Real>(I_y.get(), I, nx, ny, 1, fy, 3, 3, 1);
        
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                Real mag = sqrt(I_x[y*nx + x]*I_x[y*nx + x]
                        + I_y[y*nx + x]*I_y[y*nx + x]);
                
                // threshold
                edge[y*nx + x] = MAX(mag, threshold);
            }
        }
    
    // marr_hildreth method (seems not robust)
    } else if (method == 2) {
        int n=3;
        auto kernel_ = std::make_unique<Real[]>(n*n);
        Real *kernel = kernel_.get();
        
        gaussian_kernel(kernel, n, sigma);
        

        // Gaussian
        auto img_smooth = std::make_unique<Real[]>(nx*ny);
        corr2d(img_smooth.get(), I, nx, ny, 1, kernel, n, n, 1);

        // \nabla ^2
        auto lap = std::make_unique<Real[]>(nx*ny);
        Real op[9] = {1, 1, 1, 1, -8, 1, 1, 1, 1};
        corr2d(lap.get(), img_smooth.get(), nx, ny, 1, op, 3, 3, 1);

        // find the maximum of laplacian
        Real max=-5555.;
        for (int i=0; i < nx*ny; i++) {
            if (lap[i] > max)
                max = lap[i];
        }

        // detect zero-crossing
        for (int y=1; y < ny-1; y++) {
            for (int x=1; x < nx-1; x++) {
                Real UL = lap[ (y-1)*nx + x-1 ];
                Real UU = lap[ (y-1)*nx + x ]; 
                Real UR = lap[ (y-1)*nx + x+1 ];
                Real LL = lap[ (y)*nx   + x-1 ];
                Real RR = lap[ (y)*nx   + x+1 ];
                Real DL = lap[ (y+1)*nx + x-1 ];
                Real DD = lap[ (y+1)*nx + x   ];
                Real DR = lap[ (y+1)*nx + x+1 ];

                Real edge_value = 0.;
                if ( UU*DD < 0. && UL*DR < 0. && LL*RR <0. && UR*DL < 0.) {
                    Real max = 0;
                    max = MAX(abs(UU-DD), abs(UL-DR));
                    max = MAX(max, abs(LL-RR));
                    max = MAX(max, abs(UR-DL));
                    if (max > threshold) {
                        edge_value = max;
                    }
                }
                edge[y*nx + x] = edge_value;
            }
        }
    }
}

}

#endif

// namespace jkimg 
// {
// 	void interp2_bicubic(
// 				Real*	out,
// 		const	Real*	I,
// 		const	Real*	u,
// 		const	Real*	v,
// 		const	int		nx,
// 		const	int		ny,
// 		const	Real*	extra
// 		);

// 	void compute_gradient(
// 				Real*  I_x,
// 				Real*  I_y,
// 		const	Real*  I,
// 		const	int     method,
// 		const   int     nx,
// 		const   int     ny
// 		);

// 	// compute divergence with backward difference
// 	void compute_divergence(
// 				Real*  out,
// 		const   Real*  I1,  
// 		const   Real*  I2,
// 		const   int     nx,
// 		const   int     ny
// 		);

// 	// solve: A x = b by Gauss-Seidel: A = (I - pLHS * Laplacian), b = pRHS
// 	void compute_gauss_seidel(
// 				Real*  pData,
// 		const 	Real*  pLHS,
// 		const 	Real*  pRHS, 
// 		const	int 	nIterG,
// 		const	int 	nx,
// 		const	int 	ny
// 		);

// 	Real minFromArray(Real* arr, int size);
// 	Real maxFromArray(Real* arr, int size);
// }

// 	// #endif


