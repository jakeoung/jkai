/*
<%
import pybind11

cfg['compiler_args'] = ['-std=c++14', '-stdlib=libc++']
cfg['include_dirs'] = ['../../include']
cfg['sources'] = [
#                '../../include/jkai/flow/brox2004/brox_optic_flow.cpp
#                  '../jkai/flow/lk_inverse.cpp',
#                  '../../jkai/flow/epicflow.cpp'
                 ]

%>

*/

#include <memory>
#include <iostream>
#include <string>
#include "jkai/flow/jkflow.h"
#include "jkai/flow/brox2004/brox_optic_flow.cpp"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#ifndef Dtype
#define Dtype float
#endif

// #include <gsl/gsl_blas.h>

// struct buffer_info {
//     void *ptr;
//     size_t itemsize;
//     std::string format;
//     int ndim;
//     std::vector<size_t> shape;
//     std::vector<size_t> strides;
// };

namespace py = pybind11;

void pre_check(py::buffer_info &info) {
    if (info.ndim < 2) 
        throw std::runtime_error("The buffer dimension should be 2 or 3!");

    if (info.format != "f" && info.format != "d") {
        throw std::runtime_error("Use Dtype or double function!");
    }
}

auto copy_buf(py::buffer_info& info, Dtype* ptr)
{
    return py::buffer_info(
        ptr,
        info.itemsize,
        info.format,
        info.ndim,
        info.shape,
        info.strides
    );
}

// py::array_t<datatype> grad(py::array A) {
// auto lk_inverse(py::array I1, py::array I2,
//     int niters, int nscales, int nparams)
// {
//     py::buffer_info info = I1.request();
//     pre_check(info);

//     std::unique_ptr<JKFlow> flow;

//     py::print("~method: LKInverse");
//     flow = std::make_unique<LKInverse>(
//         (Dtype*)info.ptr, (Dtype*)I2.request().ptr,
//         info.shape[1], info.shape[0], 1, niters, nscales, nparams);


//     py::print("~call c++ function");
//     flow->compute_flow();

//     /************************
//     ** convert to a python object
//     ************************/
//     auto u = flow->get_u_ptr();
//     auto v = flow->get_v_ptr();

//     auto u_np = py::array(copy_buf(info, u));
//     auto v_np = py::array(copy_buf(info, v));

//     return py::make_tuple(u_np, v_np);
// }

// auto epicflow(py::array I1, py::array I2,
//             py::array edges, py::array matches,
//             int niters)
// {
//     py::buffer_info info = I1.request();
//     pre_check(info);
    
//     int nx, ny, nc;
//     if (info.ndim == 2) {
//         nx = info.shape[1];
//         ny = info.shape[0];
//         nc = 1;
//     } else {
//         nx = info.shape[2];
//         ny = info.shape[1];
//         nc = info.shape[0];
//     }
    
//     auto flow = std::make_unique<EpicFlow> (
//         (Dtype*)info.ptr, (Dtype*)I2.request().ptr,
//         nx, ny, nc, niters, (Dtype*)edges.request().ptr, (Dtype*)matches.request().ptr);
    
//     flow->compute_flow();

//     auto u = flow->get_u_ptr();
//     auto v = flow->get_v_ptr();

//     auto u_np = py::array(copy_buf(info, u));
//     auto v_np = py::array(copy_buf(info, v));

//     return py::make_tuple(u_np, v_np);
// }

auto brox2004(py::array I1, py::array I2, const Dtype wreg, const Dtype wgrad,
    const int nscales, const Dtype nu,
    const int niters, const int verbose)
{
    py::buffer_info info = I1.request();
    pre_check(info);

    int nx, ny, nc;
    if (info.ndim == 2) {
        nx = info.shape[1];
        ny = info.shape[0];
        nc = 1;
    } else {
        nx = info.shape[2];
        ny = info.shape[1];
        nc = info.shape[0];
    }

    if (nc > 1) {
        throw std::runtime_error("You should input the mean of color image.");
    }

    auto u = py::array_t<Dtype>(nx*ny);
    auto v = py::array_t<Dtype>(nx*ny);

    auto I1mean = py::array_t<Dtype>(nx*ny);
    auto I2mean = py::array_t<Dtype>(nx*ny);

    brox_optic_flow((Dtype*)I1.request().ptr, (Dtype*)I2.request().ptr, (Dtype*)u.request().ptr,
    (Dtype*)v.request().ptr, nx, ny, wreg, wgrad, nscales, nu, 0.0001, 1, niters, verbose);

    u.resize({ny, nx});
    v.resize({ny, nx});    

    return py::make_tuple(u, v);
}
    
auto desc = R"(Compute optical flow

Args:
    img (ndarray) : image array
    method (str, optional) : `forward` (default) for the forward finite difference method
        `backward` for the backward difference method.
            
Returns:
    2-element tuple containing

    - img_x (ndarray) : gradient image w.r.t. x-axis
    - img_y (ndarray) : gradient image w.r.t. y-axis

Example:
    >>> A = np.array([ [1, 2, 1], [3, 5, 7] ], dtype=np.Dtype32)
    >>> Ax, Ay = jkimg.compute_gradient(A, method="central")
    >>> Ax
    ([ [.5, 0., -.5], [1., 2., 1.]], dtype=np.Dtype32))
)";


PYBIND11_MODULE(jkflow, m) {
    m.doc() = "JKflow module";
    // m.def("compute_flow", &compute_flow, desc,
    //     py::arg("img1"), py::arg("img2"), py::arg("method") = "LKInverse",
    //     py::arg("niters")=30, py::arg("nscales")=1, py::arg("nparams")=3 );
    
    // m.def("lk_inverse", &lk_inverse, desc,
    //     py::arg("img1"), py::arg("img2"),
    //     py::arg("niters")=30, py::arg("nscales")=1, py::arg("nparams")=3 );    
    
    // m.def("epicflow", &epicflow, "epic flow",
    //     py::arg("img1"), py::arg("img2"), py::arg("edges"), py::arg("matches"),
    //     py::arg("niters"));

    m.def("brox2004", &brox2004, "brox method",
        py::arg("img1"), py::arg("img2"), py::arg("wreg")=1, py::arg("wgrad")=1,
        py::arg("nscales")=5, py::arg("nu")=0.75,
        py::arg("niters")=100, py::arg("verbose")=0);


    // py::class_<JKflow> jkflow(m, "jkflow");
    // jkflow
    //     .def("add", &JKflow::add);

    // py::class_<Animal> animal(m, "Animal");
    // animal
    //     .def("go", &Animal::go);

    // py::class_<Dog>(m, "Dog", animal)
    //     .def(py::init<>());

    // m.def("call_go", &call_go);
}
