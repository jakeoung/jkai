import numpy as np
import cppimport as c

c.set_quiet(False)
f = c.imp('jkflow')

A = np.array([[1,2,3,4,5],[4,5,6,7,8]], dtype=np.float32)
B = np.array([[0,1,2,3,4],[0,4,5,6,7]], dtype=np.float32)

# u, v = f.lk_inverse(A, B, 10)
# u, v = f.epicflow(A, B, 6, A, A)
u, v = f.brox2004(A, B)

print(u)