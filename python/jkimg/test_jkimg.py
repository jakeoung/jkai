import numpy as np
import cppimport as c

c.set_quiet(False)
f = c.imp('jkimg')

A = np.array([
    [[1, 2],
     [3, 4] ],
    [[1, 2],
     [3, 4] ]
], dtype=np.float32)

K = np.array([
    [0, 1, 0],
    [0, 0, 0],
    [0, -1, 0]
], dtype=np.float32)

out = f.corr2d(A, K)

# A = np.array([
#             [1, 2],
#             [11, 10]
#         ], dtype=np.float32)
        
# K = np.array([
#     [0, 1, 0],
#     [0, 0, 0],
#     [0, -1, 0]
# ], dtype=np.float32)

out = f.corr2d(A, K)
print(out)
