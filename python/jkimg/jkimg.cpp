/*
<%
import pybind11
cfg['compiler_args'] = ['-std=c++1y']
cfg['include_dirs'] = ['../../include', pybind11.get_include(), pybind11.get_include(True)]
%>
*/

#include <memory>
#include <iostream>
#include <string>
#include "jkai/jkimg.h"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

// #include <gsl/gsl_blas.h>

// struct buffer_info {
//     void *ptr;
//     size_t itemsize;
//     std::string format;
//     int ndim;
//     std::vector<size_t> shape;
//     std::vector<size_t> strides;
// };

namespace py = pybind11;

void pre_check(py::buffer_info &info) {
    if (info.ndim < 2) 
        throw std::runtime_error("Incompatible buffer dimension!");

    if (info.format != "f" && info.format != "d") {
        throw std::runtime_error("Use float or double array!");
    }
}

auto copy_buf(py::buffer_info& info)
{
    return py::buffer_info(
        nullptr,
        info.itemsize,
        info.format,
        info.ndim,
        info.shape,
        info.strides
    );
}

// py::array_t<datatype> grad(py::array A) {
// template<typename Dtype>
py::tuple compute_gradient(py::array A, const std::string& method)
{    
    py::buffer_info info = A.request();
    pre_check(info);

    auto Ax = py::array(copy_buf(info));
    auto Ay = py::array(copy_buf(info));
    
    int method_difference;
    if (method == "central") {
        method_difference = 0;
    } else if (method == "forward") {
        method_difference = 1;
    } else {
        throw std::runtime_error("Incorret method name!");
    }

    if (info.format == "f") {
        auto pA = (float *) (info.ptr);
        auto pAx = (float *) Ax.request().ptr;
        auto pAy = (float *) Ay.request().ptr;
        jkimg::compute_gradient(pAx, pAy, pA, method_difference, info.shape[1], info.shape[0]);
    } else if (info.format == "d") {
        auto pA = (double *) (info.ptr);
        auto pAx = (double *) Ax.request().ptr;
        auto pAy = (double *) Ay.request().ptr;
        jkimg::compute_gradient(pAx, pAy, pA, method_difference, info.shape[1], info.shape[0]);
    }

    return py::make_tuple(Ax, Ay);
}

auto warp_flow(py::array img, py::array u, py::array v)
{
    py::buffer_info info = img.request();
    auto warped = py::array(copy_buf(info));

    if (info.format == "f") {
        auto pImg = (float *) (info.ptr);
        auto pu = (float *) u.request().ptr;
        auto pv = (float *) v.request().ptr;
        auto pWarped = (float *) (warped.request().ptr);
        jkimg::interp2_bicubic(pWarped, pImg, pu, pv, info.shape[1], info.shape[0], (float)0.);
    } else if (info.format == "d") {
        auto pImg = (double *) (info.ptr);
        auto pu = (double *) u.request().ptr;
        auto pv = (double *) v.request().ptr;
        auto pWarped = (double *) (warped.request().ptr);
        jkimg::interp2_bicubic(pWarped, pImg, pu, pv, info.shape[1], info.shape[0], (double)0.);
    }

    return warped;
}

auto detect_edge(py::array I, const std::string& method, float threshold, float sigma)
{
    auto info = I.request();
    pre_check(info);
    int ny = info.shape[0];
    int nx = info.shape[1];

    auto edge = py::array(copy_buf(info));
    int method_;
    if (method == "sobel") {
        method_ = 1;
    } else if (method == "marr_hildreth") {
        method_ = 2;
    }

    if (info.format == "f") {
        auto pI = (float *) (info.ptr);
        auto pEdge = (float *) edge.request().ptr;
        jkimg::detect_edge(pEdge, pI, method_, nx, ny, (float)threshold, (float)sigma);
    } else if (info.format == "d") {
        auto pI = (double *) (info.ptr);
        auto pEdge = (double *) edge.request().ptr;
        jkimg::detect_edge(pEdge, pI, method_, nx, ny, (double)threshold, (double)sigma);
    }

    return edge;
}

auto corr2d(py::array A, py::array kernel)
{    
    py::buffer_info info = A.request();
    pre_check(info);

    int nx, ny, nc;
    if (info.ndim == 2) {
        nx = info.shape[1];
        ny = info.shape[0];
        nc = 1;
    } else if (info.ndim == 3) {
        nx = info.shape[2];
        ny = info.shape[1];
        nc = info.shape[0];
    }
    
    int nKx, nKy;
    py::buffer_info info_kernel = kernel.request();
    if (info_kernel.ndim == 1) {
        nKx = info_kernel.shape[0];
        nKy = 1;
    } else if (info_kernel.ndim == 2) {
        nKx = info_kernel.shape[1];
        nKy = info_kernel.shape[0];
    } else {
        throw std::runtime_error("The dimension of kernel should be less than 3");
    }
    
    auto out = py::array(copy_buf(info));
    
    if (info.format == "f") {
        auto pA = (float *) (info.ptr);
        auto pOut = (float *) out.request().ptr;
        auto pKernel = (float *) kernel.request().ptr;
        jkimg::corr2d(pOut, pA, nx, ny, nc,
            pKernel, nKx, nKy, 1);

    } else if (info.format == "d") {

        auto pA = (double *) (info.ptr);
        auto pOut = (double *) out.request().ptr;
        auto pKernel = (double *) kernel.request().ptr;
        jkimg::corr2d(pOut, pA, nx, ny, nc,
            pKernel, nKx, nKy, 1);
    }

    return py::array(out);
}

// template<typename Dtype>
// py::array interp2_bicubic_flow(py::array_t<Dtype> A)
// {
//     py::buffer_info info = A.request();

//     pre_check(info);
//     // info_copy = buf_copy(info);
//     py::print("info shape: ", info.shape[0], ", ", info.shape[1]);

//     // make return array
//     auto Ax = py::array_t<float>(buf_copy(info));
//     auto Ay = py::array_t<float>(buf_copy(info));
    
//     float* pA = (float *)(info.ptr);
//     float* pAx = (float *) Ax.request().ptr;
//     float* pAy = (float *) Ay.request().ptr;

//     py::print(sizeof(pA[0]));

//     jkimg::compute_gradient(pAx, pAy, pA, 0, info.shape[1], info.shape[0]);
    
//     return py::make_tuple(Ax, Ay);
// }

auto grad_desc = R"(Compute gradient of image.

Args:
    img (ndarray) : image array
    method (str, optional) : `forward` (default) for the forward finite difference method
        `backward` for the backward difference method.
            
Returns:
    2-element tuple containing

    - img_x (ndarray) : gradient image w.r.t. x-axis
    - img_y (ndarray) : gradient image w.r.t. y-axis

Example:
    >>> A = np.array([ [1, 2, 1], [3, 5, 7] ], dtype=np.float32)
    >>> Ax, Ay = jkimg.compute_gradient(A, method="central")
    >>> Ax
    ([ [.5, 0., -.5], [1., 2., 1.]], dtype=np.float32))
)";


PYBIND11_MODULE(jkimg, m) {
    m.doc() = "JKImg module";
    
    m.def("compute_gradient", &compute_gradient, grad_desc,
        py::arg("img"), py::arg("method") = "forward");

    m.def("detect_edge", &detect_edge, "detect edge",
        py::arg("img"), py::arg("method") = "sobel",
        py::arg("threshold") = 0.1, py::arg("sigma") = 0.5);

    m.def("corr2d", &corr2d, "correlation 2d",
        py::arg("A"), py::arg("kernel"));

    m.def("warp_flow", &warp_flow, "warp by flow",
        py::arg("img"), py::arg("u"), py::arg("v"));
}

/*
Compute gradient of image\n
Args:\n
    img (ndarray) : image array\n
    method (int) :  0, central gradient\n
                    1 (default), forward difference method\n
\n
Returns:\n
    - img_x : gradient image w.r.t. x-axis
    - img_y : gradient image w.r.t. y-axis
    "

    */
