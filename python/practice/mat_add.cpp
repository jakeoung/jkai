/*
cfg['linker_args'] = ['...']
cfg['compiler_args'] = ['-std=c++11']
cfg['libraries'] = ['gsl']
cfg['include_dirs'] = ['...']
if multiple souce files are considered,
cfg['sources'] = ['../../']
<%
cfg['include_dirs'] = ['/usr/local/include']
setup_pybind11(cfg)
%>
*/ 

// #include "pybind11/include/pybind11/pybind11.h"
// #include "pybind11/include/pybind11/numpy.h"

#include <iostream>

#include <../jkimg.h>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

// #include <gsl/gsl_blas.h>

// struct buffer_info {
//     void *ptr;
//     size_t itemsize;
//     std::string format;
//     int ndim;
//     std::vector<size_t> shape;
//     std::vector<size_t> strides;
// };

namespace py = pybind11;

py::array_t<float> mat_add(py::array A, py::array B) {
    
    // auto ptr = static_cast<float *>(info.ptr);
    py::buffer_info info = A.request();
    // cout << itemsize;
    // auto ptr = static_cast<T *>(info.ptr);
    
    int a = info.itemsize;

    auto C = py::array_t<float> (info.size);
    auto C_buf = C.request();

    int n = 1;
    for (auto r: info.shape) {
        n *= r;
    }

    // auto ptr1 = static_cast<float *>(info.ptr);
    // auto ptr2 = static_cast<
    add((float*)info.ptr, (float*)info.ptr, (float*)C_buf.ptr, n);

    return C;
    
    // int n = 1;
    // for (auto r: info.shape) {
    //   n *= r;
    // }

    // for (int i = 0; i <n; i++) {
    //     *ptr++ *= 2;
    // }
}


PYBIND11_PLUGIN(mat_add) {
    pybind11::module m("mat_add", "auto-compiled c++ extension");
    m.def("mat_add", &mat_add);
    return m.ptr();
}