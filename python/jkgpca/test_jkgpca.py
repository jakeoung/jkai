import numpy as np
import cppimport as c

c.set_quiet(False)
jkgpca = c.imp('jkgpca')

img = np.random.random([500,500]).astype(np.float32)
region = np.array([150, 150, 100, 100], dtype=np.int32)

transformed = np.array([100,100], np.float32)
#tilt = jkgpca.Tilt(img, region, transformed)

#tilt.transform_img(0.,0.)
import jkgpca
import scipy.misc as misc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

#plt.rcParams['image.cmap'] = 'gray'
img = misc.ascent()
img = (img - img.min() ) / (img.max() - img.min())
img = img.astype(np.float32)
H, W = img.shape

region = np.array([100, 100, 200, 100], dtype=np.int32)

#plt.imshow(img)

gca = plt.gca()
rect = patches.Rectangle((region[0],region[1]), region[2], region[3], linewidth='1', edgecolor='r', facecolor='None')
gca.add_patch(rect)

tilt = jkgpca.Tilt(img, region)
tilt.guess_initial(1,1)
out = tilt.do_tilt(1,0,0.1)
#tilt.do_tilt()
# help(tilt)
