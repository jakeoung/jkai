#include <iostream>
#include <string>
#include "jkai/gpca/tilt.cpp"
#include "jkai/jkmat.h"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

void pre_check(py::buffer_info &info) {
    if (info.ndim < 2) 
        throw std::runtime_error("The buffer dimension should be 2 or 3!");

    if (info.format != "f" && info.format != "d") {
        throw std::runtime_error("Use Dtype or double function!");
    }
}

auto copy_buf(py::buffer_info& info)
{
    return py::buffer_info(
        nullptr,
        info.itemsize,
        info.format,
        info.ndim,
        info.shape,
        info.strides
    );
}

class Tilt
{

public:
    int nx, ny, nc; // original image size
    int w, h; // warped image size

    int *bbox; // shape of 4, x, y, w, h. (x,y): left-top coordinates
               // (w,h): width and height of bounding box

    py::array_t<Dtype> Tau_ = py::array_t<Dtype>(9);
    Dtype* Tau  = (Dtype*)Tau_.request().ptr;
    
    Dtype *img; // shape of [nx x ny]
    // std::unique_ptr<Dtype> warped; // shape of bbox[2]*bbox[3]

    py::array_t<Dtype> tt = py::array_t<Dtype>(100);

public:
    Tilt(Dtype* img, int* bbox, int nx, int ny, int nc)
        : img(img), bbox(bbox), nx(nx), ny(ny), nc(nc)
    {
        w = bbox[2];
        h = bbox[3];
        DEBUG("TILT constructor original");
        get_tau(Tau, 0, 0);
    }

    void get_tau(Dtype* tau, Dtype theta, Dtype t)
    {
        Dtype ct = cos(theta);
        Dtype st = sin(theta);

        tau[0] = ct;
        tau[1] = t*ct - st;
        tau[2] = 0;
        tau[3] = st;
        tau[4] = t*st + ct;
        tau[5] = 0;
        tau[6] = 0;
        tau[7] = 0;
        tau[8] = 1;
    }
    
    auto guess_initial(int n_angle_steps=4, int n_t_steps=4)
    {
        Dtype theta, t;
        
        auto warped_ = py::array_t<Dtype>(w*h);
        warped_.resize({h, w});
        
        Dtype* warped = (Dtype*)warped_.request().ptr;

        Dtype angle_min = -M_PI/6;
        Dtype angle_max =  M_PI/6;
        
        Dtype t_min = -0.5;
        Dtype t_max =  0.5;

        if (n_angle_steps == 1) {
            angle_min = angle_max = 0.;
        }
        if (n_t_steps == 1) {
            t_min = t_max = 0.;
        }
        
        Dtype Frob_min = 10000.;
        Dtype angle_step = (angle_max - angle_min) / n_angle_steps;
        Dtype t_step = (t_max - t_min) / n_t_steps;

        
        int nx_warp = bbox[2];
        int ny_warp = bbox[3];
        
        Dtype Tau [9];

        Dtype theta_ = angle_min;
        Dtype t_;
        for (int i=0; i < n_angle_steps; i++) {
            t_ = t_min;
            for (int j=0; j < n_t_steps; j++) {
                
                get_tau(Tau, theta_, t_);
                transform_bbox(warped, img, Tau, bbox, nx, ny);
                Dtype Frob = get_frob_norm(warped, nx_warp, ny_warp);

                // DEBUG(Frob, theta, t);

                if (Frob < Frob_min) {
                    Frob_min = Frob;
                    theta = theta_;
                    t = t_;

                    DEBUG("Frob, theta, t", Frob, theta, t);
                }
                t_ += t_step;
            }
            theta_ += angle_step;
        }

        get_tau(Tau, theta, t);

        return warped_;
    }

    auto do_tilt(int niter1, int niter2, float lambda=1.)
    {
        auto jacobian_ = py::array_t<Dtype>(w*h*8);
        auto Jinv_= py::array_t<Dtype>(8*w*h);
        auto Ix_  = py::array_t<Dtype>(w*h);
        auto Iy_  = py::array_t<Dtype>(w*h);
        auto Y_   = py::array_t<Dtype>(w*h);
        auto JdT_ = py::array_t<Dtype>(w*h);

        auto I0_  = py::array_t<Dtype>(w*h);
        auto Itau_= py::array_t<Dtype>(w*h);
        auto E_   = py::array_t<Dtype>(w*h);
        auto temp_= py::array_t<Dtype>(w*h);
        auto Jtau_= py::array_t<Dtype>(w*h);

        Dtype* jacobian = (Dtype*)jacobian_.request().ptr;
        Dtype* Jinv = (Dtype*)Jinv_.request().ptr;
        Dtype* Ix   = (Dtype*)Ix_.request().ptr;
        Dtype* Iy   = (Dtype*)Iy_.request().ptr;
        Dtype* Y    = (Dtype*)Y_.request().ptr;
        Dtype* JdT  = (Dtype*)JdT_.request().ptr;

        Dtype* I0   = (Dtype*)I0_.request().ptr;
        Dtype* Itau = (Dtype*)Itau_.request().ptr;
        Dtype* E    = (Dtype*)E_.request().ptr;
        Dtype* temp = (Dtype*)temp_.request().ptr;
        Dtype* Jtau = (Dtype*)Jtau_.request().ptr;

        Dtype rho = 1.25;

        // init
        Dtype dTau[9];
        Dtype dTau_prev[9];

        // get_tau(Tau, theta, t);
        
        transform_bbox(Itau, img, Tau, bbox, nx, ny);
        

        Dtype F, F_prev;

        for (int iter1=0; iter1 < niter1; iter1++) {
            // DEBUG("@outer loop", iter1);

            jkimg::compute_gradient(Ix, Iy, Itau, 0, w, h);
            compute_jacobian(Jinv, jacobian, Ix, Iy, Tau, w, h);

            F_prev = jkmat::normF(Itau, w*h);
            
            for (int i=0; i < w*h; i++)
                Itau[i] /= F_prev; // normalization
            
            Dtype mu = rho / F_prev;
            
            // initialize
            auto init = [] (Dtype* arr, int n, Dtype val) {
                for (int i=0; i < n; i++)
                    arr[i]=val;
            };

            init(E, w*h, 0.);
            init(Y, w*h, 0.);
            init(dTau_prev, 9, 0.);
            init(dTau, 9, 0.);

            Dtype err;

            // normalization 
            
            /********************************/
            /********* ALM ITERATION ********/
            /********************************/
            for (int iter2=0; iter2 < niter2; iter2++) { 
                // DEBUG("@inner loop", iter2);
                /* update I0 */
                jkmat::dot(Jtau, jacobian, w*h, 8, dTau, 1);

                for (int i=0; i < w*h; i++) {
                    temp[i] = Itau[i] + Jtau[i] - E[i] + Y[i] / mu;
                }
                prox_nuclear(I0, temp, 1./mu, w, h);

                /* update E */
                for (int i=0; i < w*h; i++) {
                    E[i] = soft_shrinkage(
                        Itau[i] + Jtau[i] - I0[i] + Y[i]/mu, lambda / mu);
                }  

                /* update dTau */
                for (int p=0; p < 8; p++) {
                    Dtype sum = 0.;
                    for (int i=0; i < w*h; i++) {
                        int idx = p*w*h + i;
                        Dtype val = -Itau[i] + I0[i] + E[i] - Y[i] / mu;
                        
                        sum += Jinv[idx] * val;
                    }
                    dTau[p] = sum;
                }

                /* update Y */
                for (int i=0; i < w*h; i++) {
                    Y[i] += mu * (Itau[i] + Jtau[i] - I0[i] - E[i]);
                }

                /* update mu */
                mu *= rho;

                err = 0.;
                for (int i=0 ; i < 8; i++) {
                    // DEBUG(dTau_prev[i], dTau[i]);
                    err += fabs(dTau_prev[i] - dTau[i]);
                    dTau_prev[i] = dTau[i];
                }

                // if (iter2 % 20 == 0)
                //     DEBUG("@err: ", err, "iter: ", iter2);
                
                if (err < 1e-4) {
                    // DEBUG("@Samll error! err: ", err, "iter: ", iter2);
                    break;
                }

            }

            for (int i=0; i < 8; i++) {
                Tau[i] += dTau[i];
            }

            transform_bbox(Itau, img, Tau, bbox, nx, ny);

            F = jkmat::normF(Itau, w*h);

            DEBUG("#", iter1, "F: ", F, "Err: ", err);

            if (F > F_prev)
                break;
        }

        Itau_.resize({w, h});
        I0_.resize({w, h});
        E_.resize({w, h});
        return py::make_tuple(Itau_, I0_, E_, Tau_);
    }

    auto test()
    {

    }

    // auto pb_transform_img(float theta, float t)
    // {
    //     // DEBUG("get_tau test");
    //     Dtype Tau [9];

    //     get_tau(Tau, theta, t);
    //     transform_bbox(warped, img, Tau, bbox, nx, ny);
    // }
};


auto pb_init(Tilt &instance, py::array img, py::array bbox)
{
    py::buffer_info info = img.request();
    pre_check(info);
    py::buffer_info info_reg = bbox.request();

    int* pbbox = (int*) info_reg.ptr;
    
    if (info_reg.shape[0] != 4) {
        throw std::runtime_error("length of bbox should be 4");
    }

    new (&instance) Tilt((Dtype*)info.ptr, (int*)info_reg.ptr, info.shape[1], info.shape[0], 1);
    // new (&instance) Tilt((Dtype*)info.ptr, (int*)info_reg.ptr, info.shape[1], info.shape[0], 1);
}

auto pb_compute_perspective(py::array pts1, py::array pts2)
{
    py::buffer_info info = pts1.request();
    
    auto P = py::array_t<Dtype>(9);
    compute_perspective_transform((Dtype*)P.request().ptr, (Dtype*)info.ptr, (Dtype*)pts2.request().ptr);
    
    P.resize({3, 3});
    return P;
}

auto pb_warp_perspective(py::array warped, py::array img, py::array P, int use_inverse)
{
    py::buffer_info info = img.request();
    py::buffer_info info_w = warped.request();
    
    if (info.format == "f") {
        warp_perspective((Dtype*)warped.request().ptr, (Dtype*)info.ptr, (Dtype*)P.request().ptr,
            info.shape[1], info.shape[0], info_w.shape[1], info_w.shape[0], use_inverse);
    } else if (info.format == "d") {
        warp_perspective((double*)warped.request().ptr, (double*)info.ptr, (double*)P.request().ptr,
            info.shape[1], info.shape[0], info_w.shape[1], info_w.shape[0], use_inverse);
    }

    return warped;
}
