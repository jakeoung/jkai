/*
<%
import pybind11

cfg['compiler_args'] = ['-std=c++14', '-stdlib=libc++']
cfg['include_dirs'] = ['../../include', pybind11.get_include(), pybind11.get_include(True)]
%>
cfg['sources'] = [
                  '../jkai/gpca/tilt.cpp',
                 ]
*/

#include <memory>
#include <iostream>
#include <string>
#include "tilt.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;


// // py::array_t<datatype> grad(py::array A) {
// auto pb_tilt(py::array img, py::array bbox, int opt1, int opt2)
// {
//     py::buffer_info info = img.request();
//     pre_check(info);

//     py::buffer_info info_reg = bbox.request();
    
//     if (info_reg.shape[0] != 4) {
//         throw std::runtime_error("length of bbox should be 4");
//     }

//     // auto p_img_out = std::make_unique<Dtype[]> (info.shape[1]*info.shape[0]);
    
//     tilt((Dtype*)info.ptr, (int*)info_reg.ptr, info.shape[1], info.shape[0], 1,
//             opt1, opt2);

//     // return py::make_tuple(img_out);
// }


PYBIND11_MODULE(jkgpca, m) {
    m.doc() = "jkgpca module";

    py::class_<Tilt>(m, "Tilt", py::dynamic_attr())
        .def("__init__", &pb_init, 
            // [](Tilt &instance, py::array img, py::array bbox) {
            "init", py::arg("img"), py::arg("bbox"))

        // .def_readwrite("theta", &Tilt::theta)
        // .def_readwrite("t", &Tilt::t)
        // .def_readwrite("tt", &Tilt::tt)
        
        .def("guess_initial", &Tilt::guess_initial, py::arg("n_angle_steps")=3, py::arg("n_t_steps=")=3)
        .def("do_tilt", &Tilt::do_tilt,
            py::arg("niter1")=10, py::arg("niter2")=10, py::arg("lambda")=1.)

        .def("test", &Tilt::test)
        
        // .def("transform_img", &Tilt::pb_transform_img)

        ;


    m.def("compute_perspective", &pb_compute_perspective, "cp",
        py::arg("pts1"), py::arg("pts2") );
    m.def("warp_perspective", &pb_warp_perspective, "warp",
        py::arg("warped"), py::arg("img"), py::arg("P"), py::arg("use_inverse")=1 );
}

