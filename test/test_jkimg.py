import jkimg
import unittest
import numpy as np

def fun(x):
    return x + 1

def assert_close(A, B):
    np.testing.assert_allclose(A, B, atol=1e-5, rtol=1e-3)

class MyTest(unittest.TestCase):
    def test_compute_gradient_central(self):
        A = np.array([
            [1, 2, 1],
            [3, 5, 7]
        ], dtype=np.float32)
        Ax, Ay = jkimg.compute_gradient(A, method="central")
        
        assert_close(Ax, np.array([
            [.5, 0., -.5],
            [1., 2., 1.]
        ], dtype=np.float32))

        assert_close(Ay, np.array([
            [1., 1.5, 3.],
            [1., 1.5, 3.]
        ], dtype=np.float32))
    
    def test_compute_gradient_forward(self):
        A = np.array([
            [1, 2, 1],
            [3, 5, 7]
        ], dtype=np.float32)
        Ax, Ay = jkimg.compute_gradient(A, method="forward")
        
        assert_close(Ax, np.array([
            [1, -1, 0],
            [2, 2, 0]
        ], dtype=np.float32))

        assert_close(Ay, np.array([
            [2, 3, 6],
            [0, 0, 0]
        ], dtype=np.float32))
        
    def test_conv2d(self):
        A = np.array([
            [1, 2],
            [3, 4]
        ], dtype=np.float32)
        
        K = np.array([
            [0, 1, 0],
            [0, 0, 0],
            [0, -1, 0]
        ], dtype=np.float32)
        
        out = jkimg.conv2d(A, K)
        
        assert_close(out, np.array([
            [-2, -2],
            [-2, -2]
        ], dtype=np.float32))

    def test_conv2dChannel(self):
        A = np.array([
            [[1, 2],
             [3, 4] ],
            [[1, 2],
             [3, 4] ]
        ], dtype=np.float32)
        
        K = np.array([
            [0, 1, 0],
            [0, 0, 0],
            [0, -1, 0]
        ], dtype=np.float32)
        
        out = jkimg.conv2d(A, K)
        
        assert_close(out, np.array([
           [[-2, -2],
            [-2, -2]],
           [[-2, -2],
            [-2, -2]] 
        ], dtype=np.float32))


if __name__ == '__main__':
    unittest.main()