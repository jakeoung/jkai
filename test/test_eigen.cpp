#include <Eigen/Dense>
#include <iostream>

#define Dtype float

typedef Eigen::Map<Eigen::Matrix<Dtype, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> MapMatrix;


int main()
{

float arr [9] = {1,2,3,4,5,6,7,8,9};

MapMatrix M(arr, 3, 3);
std::cout << M;

std::cout << "\n";

std::cout << M.transpose() << "\n";

auto Mt = M.transpose();

std::cout << Mt;

//MapMatrix (arr, 3, 3) = Mt;

for (int i=0; i < 9; i++) std::cout << arr[i];

return 0;
}

