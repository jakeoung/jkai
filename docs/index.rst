JKAI for self-study
============================

Contents:

.. toctree::
   :maxdepth: 2

   jkimg
   jkflow
   jkgpca
